const TEST_REGEX = '(/__tests__/^(?!_).*|(\\.|/)(test|spec))\\.(jsx?|js?|tsx?|ts?)$'

module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js', '<rootDir>/client/testing/mocks/browserMocks.js'],
  testRegex: TEST_REGEX,
  transform: {
    '^.+\\.tsx?$': 'babel-jest'
  },
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  collectCoverage: true,
  coverageReporters: [
    "json",
    "lcov",
    "text",
    "clover",
  ],
  moduleNameMapper: {
    "\\.(css|jpg|png|svg)$": '<rootDir>/empty-module.js'
  },
  globals: {
    "ts-jest": {
      useBabelrc: true,
      tsConfigFile: "tsconfig.jest.json"
    }
  },
  moduleNameMapper: {
    "^storage(.*)$": "<rootDir>/client/storage$1",
    "^services(.*)$": "<rootDir>/client/services$1",
    "^components(.*)$": "<rootDir>/client/components$1",
    "^styles(.*)$": "<rootDir>/client/styles$1",
    "^testing(.*)$": "<rootDir>/client/testing$1",
    "^assets(.*)$": "<rootDir>/client/assets$1",
    "^pages(.*)$": "<rootDir>/client/pages$1",
  },
  collectCoverageFrom: [
    "client/**/*.{js,jsx,ts,tsx}",
    "server/**/*.{js,jsx,ts,tsx}",
    "!client/**/_*.{js,jsx,ts,tsx}",
    "!**/testing/**/*.{js,jsx,ts,tsx}"
  ]
}