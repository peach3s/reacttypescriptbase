const withTypescript = require('@zeit/next-typescript')
const withSass = require('@zeit/next-sass')
const withCss = require('@zeit/next-css');
const withImages = require('next-images');
const path = require('path');

module.exports = withSass(withCss(withImages(withTypescript({
  webpack(cfg, options) {
    const originalEntry = cfg.entry;
    
    function srcPath(subDir) {
      return path.join(__dirname, subDir);
    }

    cfg.node = {
      fs: "empty"
    }

    cfg.entry = async () => {
      const entries = await originalEntry()

      if (entries['main.js'] && !entries['main.js'].includes('./polyfills.js')) {
        entries['main.js'].unshift('./polyfills.js')
      }

      return entries
    }

    cfg.resolve.alias = {
      assets: srcPath('client/assets'),
      components: srcPath('client/components'),
      storage: srcPath('client/storage'),
      styles: srcPath('client/styles'),
      services: srcPath('client/services'),
      pages: srcPath('client/pages'),
      testing: srcPath('client/testing'),
      server: srcPath('server'),
    }

    return cfg
  }
}))))