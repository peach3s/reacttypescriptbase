import * as Prometheus from 'prom-client';
import { Request, Response } from 'express';

class MetricsProvider {
  private httpRequestDurationMicroseconds = new Prometheus.Histogram({
    name: 'http_request_duration_ms',
    help: 'Duration of HTTP requests in ms',
    labelNames: ['method', 'route', 'code'],
    buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500],  // buckets for response time from 0.1ms to 500ms
  });

  private pagesVisited = new Prometheus.Counter({
    name: 'pages_visited',
    help: 'Count of page visits',
    labelNames: ['page_title']
  });

  private metricsInterval: any;

  public Start() {
    this.metricsInterval = Prometheus.collectDefaultMetrics({
      timeout: 5000,
    });
  }

  public Stop() {
    clearInterval(this.metricsInterval);
  }

  public addRequest(req: Request, res: Response): void {
    const responseTime = Date.now() - res.locals.startEpoch;
    this.httpRequestDurationMicroseconds.labels(req.method, req.route.path, `${res.statusCode}`).observe(responseTime);
  }

  public addPageView(title: string) {
    this.pagesVisited.inc({
      page_title: title,
    });
  }

  public Registry(): Prometheus.Registry {
    return Prometheus.register;
  }
}

export default new MetricsProvider();
