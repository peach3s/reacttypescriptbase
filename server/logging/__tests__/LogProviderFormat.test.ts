import { GetFormat } from '../LogProviderFormat';
import { format } from 'winston';

describe('Log Provider Format tests', () => {
  xit('Should call format.printf when GetFormat is called', () => {
    const printfSpy = jest.spyOn(format, 'printf');
    GetFormat();
    expect(printfSpy).toBeCalled();
  })
})