import { createLogger, Logger, format, transports } from 'winston';
import * as rotateFile from 'winston-daily-rotate-file';
import { GetFormat } from './LogProviderFormat';
import Config from '../config/config';

class LogProvider {
  public logger: Logger;

  constructor() {
    this._setup();
  }

  private _setup() {
    this.logger = createLogger({
      level: 'error',
      format: format.combine(
        format.timestamp({
          format: 'HH:mm:ss',
        }),
        GetFormat(),
      ),
      transports:[
        new rotateFile({
          filename: `${Config.loggingFolder}\\%DATE%.log`,
          datePattern: 'YYYYMMDD',
        }),
      ],
    });
  }
}

export default new LogProvider().logger;

export function GetConsoleTransport() {
  return new transports.Console({
    format: format.combine(
      format.timestamp({
        format: 'HH:mm:ss',
      }),
      GetFormat(),
    ),
    level: 'silly',
  });
}
