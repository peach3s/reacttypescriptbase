import { format } from 'winston';

export function GetFormat() {
    return format.printf(({ timestamp, level, message}) => {
        const len = 8 - level.toString().length;
        let padder = "";
        if(len > 0) {
            padder = new Array(len).join(" ");
        }
        return `${timestamp} - [${level}]:${padder} ${message}`;
      });
}
