import App from './app';
import Config from './config';

export interface IApiRoute {
  method: 'GET' | 'PUT' | 'POST' | 'DELETE';
  action: any;
  route: string;
}

export class Routes {
  public static RouteList: IApiRoute[] = [];

  public routes(mainApp: App): void {
    const handle = mainApp.nextApp.getRequestHandler();
    const app = mainApp.app;
    // const nextApp = mainApp.nextApp;
    const packageVersion: string = require('../../package.json').version;

    // Pre-request stuff

    app.use((req, res, next) => {
      res.locals.startEpoch = Date.now();

      if(req.secure || !Config.isHttps) {
        next();
      } else {
        res.redirect(`https://${req.headers.host}${req.url}`);
      }
    });

    app.get('/api/version', (req, res) => {
      res.send(packageVersion);
    });

    app.get('*', (req, res) => {
      return handle(req, res);
    });
  }
}
