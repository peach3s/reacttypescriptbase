import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import { Routes } from './routes';
import * as next from 'next';

class App {
  public static version: string;

  public app: express.Application;
  public routeProvider: Routes = new Routes();
  public nextApp: next.Server;

  constructor(nextApp: next.Server) {
    this.nextApp = nextApp;
    this.app = express();

    App.version = require('../../package.json').version;
  }

  public async Setup() {
    this.config();
    this.routeProvider.routes(this);

    return this.app;
  }

  protected config(): void {
    console.log('Config called');
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(cookieParser());
  }
}

export default App;
