import App from '../app';
import NextAppMock from '../../testing/mocks/nextApp.mock';

describe('App setup tests', () => {
  let nextApp;
  let mockApp: App;

  beforeAll(() => {
    jest.doMock('next', () => {
      return () => {
        return NextAppMock;
      }
    })
    jest.mock('express', () => {
      return require('express')
    })
    nextApp = NextAppMock;
  })

  beforeEach(() => {
    mockApp = new App(nextApp);
  })

  it('Should call config on Setup', () => {
    const configSpy = jest.spyOn<any, string>(mockApp, 'config');
    mockApp.Setup();
    expect(configSpy).toBeCalled();
  })

  it('Should set up routes from route provider on Setup', () => {
    const routesSpy = jest.spyOn(mockApp.routeProvider, 'routes');
    mockApp.Setup();
    expect(routesSpy).toBeCalled();
  })

  afterEach(() => {
    mockApp = null;
  })

  afterAll(() => {
    nextApp = null;
  })
})