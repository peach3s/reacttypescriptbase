export enum LogLevel {
  VERBOSE,
  ERROR,
  INFO,
  DEBUG,
}

export default class Config {
  public static isProduction: boolean = false;
  public static port: number = 3000;
  public static logLevel: LogLevel = LogLevel.VERBOSE;
  public static isHttps: boolean = false;
  public static httpsCertKey: string = '';
  public static httpsCertPerm: string = '';
  public static loggingFolder: string = './';
}
