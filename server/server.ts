import * as next from 'next';
import { Server } from 'net';
import * as https from 'https';
import * as fs from 'fs';
import Config from './config/config';
import serverConfig from './config/app';
import LogProvider from './logging/LogProvider';

const port = Config.port;
const dev = !Config.isProduction;
const app = next({ dir: './client', dev });

app.prepare().then(() => {
  new serverConfig(app).Setup().then(application => {
    let running: Server;

    if(Config.isHttps) {
      const options = {
        key: fs.readFileSync(Config.httpsCertKey),
        cert: fs.readFileSync(Config.httpsCertPerm),
      };

      application.enable('trust proxy');

      running = https.createServer(options, application).listen(port, err => {
        if(err) {
          LogProvider.error(err);
          throw err;
        }
        LogProvider.info(`> Ready on https://localhost:${port}`);
      });
    } else {
      console.log('Starting http server');
      running = application.listen(port, err => {
        if(err) {
          LogProvider.error(err);
          throw err;
        }
        LogProvider.info(`> Ready on http://localhost:${port}`);
      });
    }

    process.on('SIGTERM', () => {
      running.close(err => {
        if(err) {
          console.error(err);
          process.exit(1);
        }

        process.exit(0);
      });
    });
  });
});
