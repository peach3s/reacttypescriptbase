# Fusion Smartboard app using Next.js

## Index
- [Installation](#installation)
- [Useful Links](#useful-links)
- [Snippets](#snippets)
    - [React Component](#normal-react-component)
    - [React Component - With Store](#component-with-store)
    - [Store Manipulation](#manipulating-the-application-store)

## Installation

You will need Node installed on your machine. You can get it [here](https://nodejs.org/dist/v10.15.0/node-v10.15.0-x64.msi).

Next you will need to get Yarn. You can download it from [here](https://yarnpkg.com/latest.msi).

Once you have both of these, you will need to run the below commands on the root of the project.

```posh
::This is to install the dependencies for the project

yarn

::Once they are installed you can run this one to start a local dev server

yarn dev

```

For the configs we are currently using a json file within ProgramData.

In the dev environment you will need to create this yourself. There is a server-config.json in the root of the project that you will need to copy
to C:\ProgramData\CRBCunninghams\Impact\Smartboard.

You should edit this to include your own data server address.


## Useful Links

I will try to update this with any tutorials or the rest that I find. Currently it will just be the main pages of the libraries and languages that are being used for it.

- [Next.js](https://nextjs.org/) - Next.js homepage with docs and tutorials
- [Undux](https://undux.org/) - Cut down version of redux without all the extra gubbins
- [Typescript](https://www.typescriptlang.org/) - Documentation and quick start for typescript
- [SASS/SCSS](https://sass-lang.com/) - Better CSS. Has variables and better parent child management for styles. File endings should be .scss
- [React](https://reactjs.org/) - React homepage with docs and tutorials
- [Bootstrap React](https://react-bootstrap.github.io/) - Bootstrap React homepage with docs and tutorials
---

## Snippets

Creating components that you want to have access to the Undux store (application state storage) need to be created a different way that you would normally create a React component.

**Normal React component**

``` javascript
import * as React from 'react';

class Component extends React.Component {
  render() {
    return <div>Content</div>
  }
}

export default Component;
```

**Component with store**

``` javascript
import * as React from 'react';
import Store, { StoreProps } from 'storage/store';

interface IComponentProps extends StoreProps {
  // Additional passed props
}

class Component extends React.Component<IComponentProps> {
  constructor(props: IComponentProps) {
    // Store can be accessed like this
    let whatever = this.props.store.get('Whatever');
  }

  render() {
    return <div>Content</div>
  }
}

export default Store.withStore(Component);
```

**Manipulating the application store**

This is done through the [Undux](https://undux.org) library.

The state model can be found in storage/StateModel.ts

``` javascript
// This is not actually the same model as we are using

export default interface State {
  // Whatever values that you want to save in the state, with their types
  Whatever: string | null,
  Else: number | null,
  // Hydrated is just set to make sure that the values get rehydrated on page reload
  _hydrated: boolean
}
```

Setting values

``` javascript
// Setting values
this.props.store.set('Whatever')('Matters');

// This results in an error as it is setting the value to an incorrect type
this.props.store.set('Else')('incorrect');

// You also cannot set values to something that isn't in the StateModel
this.props.store.set('NoThere')(true);
```

Getting values

``` javascript
// Getting values
this.props.store.get('Else');

// Getting a value that doesn't exist results in an undefined
this.props.store.get('Nothing');
```
---

## EventHub

The solution implements an EventHub. This allows components to subscribe and unsubscribe to the EventHub. 

EventHub works like a global event handler. You subscribe to a 'Room' with a function that you want called whenever that room is called.

**Subscribing to a room**

You subscribe to a room like so

``` javascript

EventHub.SubscribeTo('NAME_OF_ROOM', functionToCall);

// You can also subscribe with an ID so you can unsubscribe easily

EventHub.SubscribeTo('NAME_OF_ROOM', functionToCall, 'id-of-subscription');

// The event hub can be subscribed to with anonymous functions as well

EventHub.SubscribeTo('NAME_OF_ROOM', (data) => { workWithData(data) });

```

Subscriptions should be done in the constructor of the object. Because of the way that these are being called you may have to bind the constructors
state to the method being called like so.

``` javascript

constructor(props) {
  // Bind state of the called method
  this.methodToBeCalled = this.methodToBecalled.bind(this);

  // Then subscribe to the room
  EventHub.SubscribeTo('NAME_OF_ROOM', methodToBeCalled, 'Component_Subscription_ID');
}

public methodToBeCalled(dataPassed) {
  console.log(dataPassed);
}

```

**Firing events**

You fire events from a room like so

``` javascript

// Without parameters; Will just call all subscribed items
EventHub.Fire('NAME_OF_ROOM');

// With parameters
EventHub.Fire('NAME_OF_ROOM', data);

EventHub.Fire('NAME_OF_ROOM', { some: "info", id: 0 });

```

**Cleaning Up**

When a component is being unmounted you should clean up any subscriptions that have been made from that component.

This is done using the following methods

``` javascript

// If you assigned an ID to the subscription when you created it
EventHub.UnsubscribeFrom('ROOM_NAME', 'id-of-subscription');

// This will clear all subscriptions from a room. Only use this if the subscriptions are only to be used in that component
EventHub.ClearRoom('ROOM_NAME');

// You should do it in the following method

async componentWillUnmount() {
  EventHub.ClearRoom('ROOM_NAME');
}

```

**To Note**

There is no hard setting of types so when you pass data through the Fire method you should make sure that it is correct for the
subscription method being called. This allows you to cherry pick what you actually want from it and also to not require any data
in which case it will just ignore anything being sent.

**Example**

``` javascript

class Component extends React.Component {
  constructor(props) {
    super(props);

    // Bind state of the called method
    this.methodToBeCalled = this.methodToBecalled.bind(this);

    // Then subscribe to the room
    EventHub.SubscribeTo('NAME_OF_ROOM', methodToBeCalled, 'Component_Subscription_ID');
  }

  public methodToBeCalled(dataPassed) {
    // This method is called when EventHub.Fire is called and will log out the data passed
    console.log(dataPassed);
  }

  async componentWillUnmount() {
    // Unsubscribing when the component is unmounted
    EventHub.UnsubscribeFrom('NAME_OF_ROOM', 'Component_Subscription_ID');
  }
}

// Somewhere else
EventHub.Fire('NAME_OF_ROOM', { data: "to", be: "passed", id: 0 });

```