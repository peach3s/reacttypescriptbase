import includes from 'core-js/library/fn/string/virtual/includes'
import repeat from 'core-js/library/fn/string/virtual/repeat'
import assign from 'core-js/library/fn/object/assign'
import findIndex from 'core-js/library/fn/array/virtual/find-index'

if(!String.includes) {
  String.prototype.includes = includes
}

if(!String.repeat) {
  String.prototype.repeat = repeat
}

if(!Object.assign) {
  Object.assign = assign
}

if(!Array.findIndex) {
  Array.prototype.findIndex = findIndex
}