import { Effects, Store, createConnectedStore } from 'undux';
import PersistPlugin from 'storage/PersistPlugin';
import State, { initialState } from 'storage/StateModel';

export let isHydrated: boolean = false;

export default createConnectedStore(initialState, PersistPlugin.plugin);

export interface IStoreProps {
  store: Store<State>;
}

export interface IStoreEffects {
  effects: Effects<State>;
}

export function rehydrateData(localStorage: Storage): State {
  const storageValues: State = initialState;

  let stateStore: string | null;
  stateStore = localStorage.getItem("StateStore");
  if(stateStore) {
    const stateStoreObj: State = JSON.parse(stateStore);
    isHydrated = true;
    return { ...storageValues, ...stateStoreObj };
  }

  isHydrated = true;

  return storageValues;
}
