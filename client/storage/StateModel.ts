export default interface IState {
  _hydrated: boolean;
}

export const initialState: IState = {
  _hydrated: false,
};
