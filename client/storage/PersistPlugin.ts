import State from 'storage/StateModel';
import { Effects, StoreDefinition } from 'undux';

class PersistPlugin {
  public plugin: Effects<State> = store => {
    store.onAll().subscribe(() => this.SetLocalStorage(store));

    return store;
  }

  public SetLocalStorage(store: StoreDefinition<State>) {
    if(typeof window !== 'undefined' && typeof store !== 'undefined') {
      const snapshot = store.getCurrentSnapshot();
      localStorage.setItem("StateStore", JSON.stringify(snapshot.getState()));
    }
  }
}

export default new PersistPlugin();
