import { rehydrateData } from 'storage/store';
import { StoreMockFactory } from 'testing/mocks/Store.mock';
import { iStateMock } from 'testing/mocks/State.mock';
import { initialState } from 'storage/StateModel';
import PersistPlugin from 'storage/PersistPlugin';

describe('Store tests', () => {
  let store;
  let storeMockFactory = new StoreMockFactory();

  beforeEach(() => {
    store = storeMockFactory.createWithEffects(iStateMock, PersistPlugin.plugin);
    PersistPlugin.SetLocalStorage(store);
  })

  afterEach(() => {
    store = null;
  })

  it('Should return correct data when local storage is set from state using rehydrateData', () => {
    const expected = store.getCurrentSnapshot().getState();
    const actual = rehydrateData(localStorage);

    expect(actual).toEqual(expected);
  })
  
  it('Should return initial state if there is no data in localStorage to rehydrate', () => {
    localStorage.clear();

    const actual = rehydrateData(localStorage);

    expect(actual).toEqual(initialState);
  })
})