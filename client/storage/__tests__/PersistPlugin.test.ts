import PersistPlugin from '../PersistPlugin';
import { StoreMockFactory } from 'testing/mocks/Store.mock';
import { iStateMock } from 'testing/mocks/State.mock';

describe('Persistant storage tests', () => {
  const storeMockFactory = new StoreMockFactory();
  let store;
  let spy;

  beforeEach(() => {
    store = storeMockFactory.createWithEffects(iStateMock, PersistPlugin.plugin);
    PersistPlugin.SetLocalStorage(store);
    spy = jest.spyOn(PersistPlugin, 'SetLocalStorage');
  })

  afterEach(() => {
    store = null;
    spy.mockClear();
  })

  it('Should call SetLocalStorage on state change', () => {
    store.set('_hydrated')(true);
    expect(spy).toBeCalled();
  })

  it('Should update the stored state in localStorage on store change', async () => {
    let storedState = JSON.parse(localStorage.getItem('StateStore'));
    expect(storedState._hydrated).toEqual(iStateMock._hydrated);
    await store.set('_hydrated')(true);
    let nStoredState = JSON.parse(localStorage.getItem('StateStore'));
    expect(nStoredState._hydrated).toEqual(true);
  })

  it('Does not fall over if window is not defined', () => {
    window = undefined;
    expect(spy).not.toThrow();
  })
})