interface IRoom {
  name: string;
  subscribers: Array<ISubscriber>;
}

interface ISubscriber {
  name: string;
  action: (params?: any) => void;
}

class EventHub {
  private rooms: Array<IRoom>;

  constructor() {
    this.rooms = [];
  }

  public Register(toRegister: string) {
    this.rooms.push({
      name: toRegister,
      subscribers: [],
    });
  }

  public SubscribeTo(roomName: string, subscribeAction: (params?: any) => void, subscribeAs?: string) {
    // console.log(`EVENT_HUB: Subscribing to ${roomName} with ${subscribeAs || 'no ident'}`)

    let roomIndex = this.GetRoomIndex(roomName);

    if(roomIndex === -1) {
      this.Register(roomName);
      roomIndex = this.GetRoomIndex(roomName);
    }

    const room = this.rooms[roomIndex];
    this.rooms[roomIndex].subscribers.push({
      name: subscribeAs || `${room.name}_sub_${room.subscribers.length}`,
      action: subscribeAction,
    });
  }

  public Fire(roomName: string, params?: any) {
    const roomIndex = this.GetRoomIndex(roomName);

    if(roomIndex > -1) {
      this.rooms[roomIndex].subscribers.forEach(sub => {
        sub.action(params);
      });
    }
  }

  public UnsubscribeFrom(roomName: string, subscriber: string) {
    const roomIndex = this.GetRoomIndex(roomName);

    if(roomIndex > -1) {
      const subIndex = this.rooms[roomIndex].subscribers.findIndex(s => s.name === subscriber);
      if(subIndex > -1) {
        this.rooms[roomIndex].subscribers.splice(subIndex, 1);
      }
    }
  }

  public ClearRoom(roomName: string) {
    const roomIndex = this.GetRoomIndex(roomName);

    if(roomIndex > -1) {
      this.rooms[roomIndex].subscribers = [];
    }
  }

  public Clean() {
    // Clears all the rooms
    this.rooms = [];
  }

  public HasRoom(roomName: string): boolean {
    const roomIndex = this.GetRoomIndex(roomName);

    return roomIndex > -1;
  }

  public RoomHasSub(roomName: string, subName: string): boolean {
    return this.GetSubs(roomName).map(sub => sub.name).includes(subName);
  }

  public RoomIsEmpty(roomName: string): boolean {
    return this.GetSubs(roomName).length === 0;
  }

  public GetSubs(roomName: string): ISubscriber[] {
    const roomIndex = this.GetRoomIndex(roomName);
    let subs = [];

    if(roomIndex > -1) {
      subs = this.rooms[roomIndex].subscribers;
    }

    return subs;
  }

  private GetRoomIndex(roomName: string): number {
    return this.rooms.findIndex(r => r.name === roomName);
  }
}

export default new EventHub();
