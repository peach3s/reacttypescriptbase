import EventHub from 'services/EventHub';
import * as sinon from 'sinon';

describe('Event Hub tests', () => {
  let hub = EventHub;

  it('Should create a room if there is none on Subscribe', () => {
    expect(hub.HasRoom('TEST_ROOM')).toBeFalsy();
    hub.SubscribeTo('TEST_ROOM', () => {});
    expect(hub.HasRoom('TEST_ROOM')).toBeTruthy();
  })

  it('Should create a room on register being called', () => {
    hub.Register('REGISTER_TEST');
    expect(hub.HasRoom('REGISTER_TEST')).toBeTruthy();
  })

  it('Should return false on has room if there is no room there', () => {
    expect(hub.HasRoom('NOT_A_ROOM')).toBeFalsy();
  })

  it('Should call subscribed function on room being fired', () => {
    let spy = sinon.stub();
    hub.SubscribeTo('FIRE_TEST', spy);
    hub.Fire('FIRE_TEST');
    expect(spy.called).toBeTruthy();
  })

  it('Should not fall over when Fire is called on a room that does not exist', () => {
    expect(() => {hub.Fire('NOT_A_ROOM')}).not.toThrow();
  })

  it('Should not fall over when UnsubscribeFrom is called on a room that does not exist', () => {
    expect(() => {hub.UnsubscribeFrom('NOT_A_ROOM', 'NOT_A_SUB')}).not.toThrow();
  })

  it('Should not fall over when UnsubscribeFrom is called on a sub that does not exist in a room that does exist', () => {
    hub.Register('IS_A_ROOM');
    expect(() => {hub.UnsubscribeFrom('IS_A_ROOM', 'NOT_A_SUB')}).not.toThrow();
  })

  it('Should not fall over when ClearRoom is called on a room that does not exist', () => {
    expect(() => {hub.ClearRoom('NOT_A_ROOM')}).not.toThrow();
  })

  it('Should return an empty array on calling GetSubs on a room that does not exist', () => {
    expect(hub.GetSubs('NOT_A_ROOM')).toEqual([]);
  })

  it('Should unsubscribe from a room when UnsubscribeFrom is called with the correct id', () => {
    hub.SubscribeTo('UNSUB_TEST', () => {}, 'TEST_ID');
    expect(hub.RoomHasSub('UNSUB_TEST', 'TEST_ID')).toBeTruthy();
    hub.UnsubscribeFrom('UNSUB_TEST', 'TEST_ID');
    expect(hub.RoomHasSub('UNSUB_TEST', 'TEST_ID')).toBeFalsy();
  })

  afterEach(() => {
    hub.Clean();
  })
})