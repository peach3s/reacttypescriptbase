
import favicon from 'assets/images/favicon.ico';
import Document, { Head, Main, NextScript } from 'next/document';

export default class FusionDocument extends Document {
  public static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  public render() {
    return (
      <html>
        <Head>
          <style>{`body {margin: 0}`}</style>

          <link rel="shortcut icon" href={favicon} />

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
