import * as React from "react";
import LoginBox, {
  ILoginResponse,
} from "crbc-web-toolbox/dist/components/LoginBox";

export class IndexPage extends React.Component {
  private async handlePinLogin(pin: string): Promise<ILoginResponse> {
    console.log(pin);

    return {
      message: "PinLogin_Failed",
      status: "failure",
    };
  }

  public render() {
    return <LoginBox attemptPinLogin={this.handlePinLogin} />;
  }
}

export default IndexPage;
