import App from "next/app";
import React from "react";
import Store, { rehydrateData, IStoreProps } from "storage/store";
import State, { initialState } from "storage/StateModel";
import "styles/app.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "crbc-web-toolbox/dist/styles/toolbox-styles-concat.css";

interface IGetInitialPropsProps extends IStoreProps {
  Component: any;
  router: any;
  ctx: any;
  version: string;
}

interface IAppState {
  iStore: State;
  title: string;
}

export default class FusionApp extends App<{}, IAppState> {
  public static async getInitialProps(props: IGetInitialPropsProps) {
    let pageProps = {};

    if (props.Component.getInitialProps) {
      pageProps = await props.Component.getInitialProps(props.ctx);
    }

    return {
      pageProps: { ...pageProps },
    };
  }

  private _iState: State = initialState;
  get iState(): State {
    return this._iState;
  }
  set iState(state: State) {
    this._iState = state;
  }

  constructor(props: any) {
    super(props);

    this.state = {
      iStore: initialState,
      title: `CRB React Web Base`,
    };
  }

  public async componentWillMount() {
    if (typeof window !== "undefined") {
      this.iState = rehydrateData(localStorage);
    }
  }

  public render() {
    const { Component, pageProps } = this.props;
    return (
      <Store.Container initialState={this.iState}>
        <title>{this.state.title}</title>
        <Component {...pageProps} />
      </Store.Container>
    );
  }
}
