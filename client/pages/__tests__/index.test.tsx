import React from 'react';
import { shallow } from 'enzyme';
import Index from 'pages/index';

describe('Index page tests', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Index />);
  })

  it('Index should have the base message when it loads', () => {
    expect(wrapper.find('h5').text()).toEqual('Hello World');
  })

  afterEach(() => {
    wrapper = null;
  })
})