import { Store, createStore, Effects } from 'undux';
import { iStateMock } from './State.mock';
import State from 'storage/StateModel';

export default createStore(iStateMock);
export interface StoreProps {
  store: Store<State>
}

export class StoreMockFactory {
  create(passedState?: State) {
    return createStore(passedState || iStateMock);
  }

  createWithEffects(passedState?: State, effect?: Effects<State>) {
    let base = createStore(passedState || iStateMock);
    return effect(base);
  }
}