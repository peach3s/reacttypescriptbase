import Router, { EventChangeOptions } from 'next/router';
import * as url from 'url';

Router.router = {
  push: async (url: string | url.UrlObject | url.Url, as?: string | url.UrlObject | url.Url, options?: EventChangeOptions) => true
}

export default Router;